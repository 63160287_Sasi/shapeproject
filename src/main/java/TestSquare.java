/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(4);
        System.out.println("Area of square1(s = " + square1.getSide()+ ") is " + square1.calArea());
        square1.setSide(3);
        System.out.println("Area of square1(s = " + square1.getSide()+ ") is " + square1.calArea());
        square1.setSide(0);
        System.out.println("Area of square1(s = " + square1.getSide()+ ") is " + square1.calArea());
    }
}
