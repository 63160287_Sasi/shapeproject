/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(2,3);
        System.out.println("Area of triangle1((h,b) = (" + triangle1.getHeight() + "," + triangle1.getBase() + ")) is " + triangle1.calArea());
        triangle1.setHeight(3);
        System.out.println("Area of triangle1((h,b) = (" + triangle1.getHeight() + "," + triangle1.getBase() + ")) is " + triangle1.calArea());
        triangle1.setBase(5);
        System.out.println("Area of triangle1((h,b) = (" + triangle1.getHeight() + "," + triangle1.getBase() + ")) is " + triangle1.calArea());
        triangle1.setHeight(0);
        System.out.println("Area of triangle1((h,b) = (" + triangle1.getHeight() + "," + triangle1.getBase() + ")) is " + triangle1.calArea());
    }
}
