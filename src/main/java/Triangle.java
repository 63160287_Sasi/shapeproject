/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class Triangle {
    private double h;
    private double b;
    public Triangle(double h,double b){
        this.h = h;
        this.b = b;
    }
    public double calArea(){
        return 0.5 * h * b;
    }
    public double getHeight(){
        return h;
    }
    public double getBase(){
        return b;
    }
    public void setHeight(double h){
        if(h <= 0){
            System.out.println("Error: Height must more than zero!!!");
            return;
        }
        this.h = h;
    }
    public void setBase(double b){
        if(b <= 0){
            System.out.println("Error: Base must more than zero!!!");
            return;
        }
        this.b = b;
    }
}
