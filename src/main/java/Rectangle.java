/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class Rectangle {
    private double h;
    private double w;
    public Rectangle(double h,double w){
        this.h = h;
        this.w = w;
    }
    public double calArea(){
        return h * w;
    }
    public double getHeight(){
        return h;
    }
    public double getWigth(){
        return w;
    }
    public void setHeight(double h){
        if(h <= 0){
            System.out.println("Error: Height must more than zero!!!");
            return;
        }
        this.h = h;
    }
    public void setWigth(double w){
        if(w <= 0){
            System.out.println("Error: Width must more than zero!!!");
            return;
        }
        this.w = w;
    }
}
