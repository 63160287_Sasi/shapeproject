/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class Square {
    private double s;
    public Square(double s){
        this.s = s;
    }
    public double calArea(){
        return s * s;
    }
    public double getSide(){
        return s;
    }
    public void setSide(double s){
        if(s <= 0){
            System.out.println("Error: Side must more than zero!!!");
            return;
        }
        this.s = s;
    }
}
